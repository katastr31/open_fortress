# open_fortress
An unofficial mirror of the OpenFortress SVN. Includes easy installation (No SVN checkout required, just clone or download the zip and rename it)

# Download the Game
Note: 1+ GB download

## I have Git installed already
- Open sourcemods folder
- Open Git Bash in this folder
- Run `git clone --depth 1 https://github.com/ordohereticus/open_fortress.git`
- Launch from Steam

## I do not have Git installed
- Download .zip file from this repo
- extract to sourcemods folder
- ENSURE FOLDER IS NAMED "open_fortress" NOT "open_fortress-master"
- Launch from Steam


# Updating the Game

## Via Git
- Open open_fortress folder
- Open Git Bash inside open_fortress folder
- Run "git pull"

## Via .zip File
- Redownload the .zip file from this url: "https://github.com/ordohereticus/open_fortress/archive/master.zip"
- Extract and rename folder

**NOTE:** This will overwrite your existing settings!
