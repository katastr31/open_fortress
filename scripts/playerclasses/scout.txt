//
// Team Fortress - Scout Player Class
//
PlayerClass
{
	// Info.
	"name"				"scout"
	"localize_name"		"TF_Class_Name_Scout"
	
	// Models.
	"model"				"models/player/scout.mdl"
	"arm_model"			"models/weapons/c_models/c_scout_arms.mdl"
	"bot_model"			"models/bots/scout/bot_scout.mdl"
	"alt_model"			"models/player/scout.mdl"
	"alt_arm_model"		"models/weapons/c_models/c_scout_arms.mdl"
	
	"CapMultiplier"		"2"
	"MaxAirDashCount"	"1"
	
	"ViewVector"		"65"
	
	"jump_sound"		"Scout.Jumpsound"
	
	// These are used in the class selection
	"ClassSelectImageRed"			"class_sel_sm_scout_red"
	"ClassSelectImageBlue"			"class_sel_sm_scout_blu"
	"ClassSelectImageMercenary"		"class_sel_sm_scout_mercenary"
	
	// These are used in your player icon next to your health
	"ClassImageRed"					"../hud/class_scoutred"
	"ClassImageBlue"				"../hud/class_scoutblue"
	"ClassImageMercenary"			"../hud/class_scoutmercenary"
	"ClassImageColorless"			"../hud/colorless/class_scoutcolorless"
	
	// Stats.
	"speed_max"			"400"
	"health_max"		"125"
	"armor_max"			"0"

	// Grenades.
	"grenade1"			"TF_GRENADE_CALTROP"
	"grenade2"			"TF_GRENADE_CONCUSSION"
	"grenade3"			"NONE"

	// Weapons.
	"WeaponCount"		"3"
	"weapon1"			"TF_WEAPON_BAT"
	"weapon2"			"TF_WEAPON_PISTOL_SCOUT"
	"weapon3"			"TF_WEAPON_SCATTERGUN"
	"weapon4"			"NONE"
	"weapon5"			"NONE"
	"weapon6"			"NONE"
	"weapon7"			"NONE"
	"weapon8"			"NONE"
	"weapon9"			"NONE"
	
	// Buildables.
	"buildable1"		"OBJ_SENTRYGUN"
	"buildable2"		"OBJ_DISPENSER"
	"buildable3"		"OBJ_TELEPORTER"

	// Death Sounds.
	"sound_death"					"Scout.Death"
	"sound_crit_death"				"Scout.CritDeath"
	"sound_melee_death"				"Scout.MeleeDeath"
	"sound_explosion_death"			"Scout.ExplosionDeath"	
	
	// Ammo.
	AmmoMax
	{
		"TF_AMMO_METAL"				"200"
	}
	
	"TFC"
	{
		"ClassImageRed"					"../hud/class_classic_scout_red"
		"ClassImageBlue"				"../hud/class_classic_scout_blue"
		"ClassImageMercenary"			"../hud/class_classic_scout_red"
		"ClassImageColorless"			"../hud/class_classic_scout_red"
		
		"model"			"models/player/tfc/scout_tfc.mdl"
		"arm_model"		"models/weapons/c_models/c_tfc_scout_arms.mdl"
		
		"speed_max"		"400"
		"health_max"	"75"
		"armor_max"		"50"
		
		"WeaponCount"	"3"
		"weapon1"		"TFC_WEAPON_CROWBAR"
		"weapon2"		"TFC_WEAPON_SHOTGUN_SB"
		"weapon3"		"TFC_WEAPON_NAILGUN"
		"weapon4"		"NONE"
		"weapon5"		"NONE"
		"weapon6"		"NONE"
		"weapon7"		"NONE"
		"weapon8"		"NONE"
		"weapon9"		"NONE"	
	}
	
	"Zombie"
	{
		"ClassImageRed"					"../hud/class_scout_zombie_red"
		"ClassImageBlue"				"../hud/class_scout_zombie_blue"
		"ClassImageMercenary"			"../hud/class_scout_zombie_red"
		"ClassImageColorless"			"../hud/class_scout_zombie_red"
		
		"model"		"models/player/zombie/scout_zombie.mdl"
		"arm_model"	"models/weapons/c_models/c_zombie_scout_arms.mdl"
		
		// Weapons.
		"WeaponCount"		"1"
		"weapon1"			"TF_WEAPON_CLAWS"
		"weapon2"			"NONE"
		"weapon3"			"NONE"
		"weapon4"			"NONE"
		"weapon5"			"NONE"
		"weapon6"			"NONE"
		"weapon7"			"NONE"
		"weapon8"			"NONE"
		"weapon9"			"NONE"
	}
}
