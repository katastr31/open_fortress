//--------------------------------------------------------------------------------------------------------------
// Civilian Response Rule File
//--------------------------------------------------------------------------------------------------------------

Criterion "CivilianIsKillSpeechObject" "CivilianKillSpeechObject" "1" "required" weight 0
Criterion "CivilianIsNotStillonFire" "CivilianOnFire" "!=1" "required" weight 0
Criterion "CivilianIsStillonFire" "CivilianOnFire" "1" "required" weight 0
Criterion "CivilianNotKillSpeech" "CivilianKillSpeech" "!=1" "required" weight 0
Criterion "CivilianNotKillSpeechMelee" "CivilianKillSpeechMelee" "!=1" "required" weight 0
Criterion "CivilianNotSaidHealThanks" "CivilianSaidHealThanks" "!=1" "required"
Criterion "IsHelpCapCivilian" "CivilianHelpCap" "1" "required" weight 0


Response PlayerCloakedSpyDemomanCivilian
{
	scene "scenes/Player/Civilian/low/cloakedspyidentify05.vcd" 
}
Rule PlayerCloakedSpyDemomanCivilian
{
	criteria ConceptPlayerCloakedSpy IsCivilian IsOnDemoman
	Response PlayerCloakedSpyDemomanCivilian
}

Response PlayerCloakedSpyEngineerCivilian
{
	scene "scenes/Player/Civilian/low/cloakedspyidentify07.vcd" 
}
Rule PlayerCloakedSpyEngineerCivilian
{
	criteria ConceptPlayerCloakedSpy IsCivilian IsOnEngineer
	Response PlayerCloakedSpyEngineerCivilian
}

Response PlayerCloakedSpyHeavyCivilian
{
	scene "scenes/Player/Civilian/low/cloakedspyidentify03.vcd" 
}
Rule PlayerCloakedSpyHeavyCivilian
{
	criteria ConceptPlayerCloakedSpy IsCivilian IsOnHeavy
	Response PlayerCloakedSpyHeavyCivilian
}

Response PlayerCloakedSpyMedicCivilian
{
	scene "scenes/Player/Civilian/low/cloakedspyidentify06.vcd" 
}
Rule PlayerCloakedSpyMedicCivilian
{
	criteria ConceptPlayerCloakedSpy IsCivilian IsOnMedic
	Response PlayerCloakedSpyMedicCivilian
}

Response PlayerCloakedSpyPyroCivilian
{
	scene "scenes/Player/Civilian/low/cloakedspyidentify04.vcd" 
}
Rule PlayerCloakedSpyPyroCivilian
{
	criteria ConceptPlayerCloakedSpy IsCivilian IsOnPyro
	Response PlayerCloakedSpyPyroCivilian
}

Response PlayerCloakedSpySniperCivilian
{
	scene "scenes/Player/Civilian/low/cloakedspyidentify08.vcd" 
}
Rule PlayerCloakedSpySniperCivilian
{
	criteria ConceptPlayerCloakedSpy IsCivilian IsOnSniper
	Response PlayerCloakedSpySniperCivilian
}

Response PlayerCloakedSpyCivilianCivilian
{
	scene "scenes/Player/Civilian/low/cloakedspyidentify02.vcd" 
}
Rule PlayerCloakedSpyCivilianCivilian
{
	criteria ConceptPlayerCloakedSpy IsCivilian IsOnCivilian
	Response PlayerCloakedSpyCivilianCivilian
}

Response PlayerCloakedSpySpyCivilian
{
	scene "scenes/Player/Civilian/low/cloakedspyidentify09.vcd" 
}
Rule PlayerCloakedSpySpyCivilian
{
	criteria ConceptPlayerCloakedSpy IsCivilian IsOnSpy
	Response PlayerCloakedSpySpyCivilian
}


//--------------------------------------------------------------------------------------------------------------
// Auto Speech
//--------------------------------------------------------------------------------------------------------------
Response HealThanksCivilian
{
	scene "scenes/Player/Civilian/low/ThanksForTheHeal01.vcd" 
	scene "scenes/Player/Civilian/low/ThanksForTheHeal02.vcd" 
	scene "scenes/Player/Civilian/low/ThanksForTheHeal03.vcd" 
}
Rule HealThanksCivilian
{
	criteria ConceptMedicChargeStopped IsCivilian SuperHighHealthContext CivilianNotSaidHealThanks 50PercentChance
	ApplyContext "CivilianSaidHealThanks:1:20"
	Response HealThanksCivilian
}

Response PlayerRoundStartCivilian
{
	scene "scenes/Player/Civilian/low/battlecry01.vcd" predelay "1.0, 5.0"
	scene "scenes/Player/Civilian/low/battlecry02.vcd" predelay "1.0, 5.0"
	scene "scenes/Player/Civilian/low/battlecry03.vcd" predelay "1.0, 5.0"
	scene "scenes/Player/Civilian/low/battlecry04.vcd" predelay "1.0, 5.0"
	scene "scenes/Player/Civilian/low/battlecry05.vcd" predelay "1.0, 5.0"
	scene "scenes/Player/Civilian/low/battlecry06.vcd" predelay "1.0, 5.0"
}
Rule PlayerRoundStartCivilian
{
	criteria ConceptPlayerRoundStart IsCivilian
	Response PlayerRoundStartCivilian
}

Response PlayerCappedIntelligenceCivilian
{
	scene "scenes/Player/Civilian/low/autocappedintelligence01.vcd" 
	scene "scenes/Player/Civilian/low/autocappedintelligence02.vcd" 
	scene "scenes/Player/Civilian/low/autocappedintelligence03.vcd" 
}
Rule PlayerCappedIntelligenceCivilian
{
	criteria ConceptPlayerCapturedIntelligence IsCivilian
	Response PlayerCappedIntelligenceCivilian
}

Response PlayerCapturedPointCivilian
{
	scene "scenes/Player/Civilian/low/autocappedcontrolpoint01.vcd" 
	scene "scenes/Player/Civilian/low/autocappedcontrolpoint02.vcd" 
	scene "scenes/Player/Civilian/low/autocappedcontrolpoint03.vcd" 
}
Rule PlayerCapturedPointCivilian
{
	criteria ConceptPlayerCapturedPoint IsCivilian
	Response PlayerCapturedPointCivilian
}

Response PlayerSuddenDeathCivilian
{
	scene "scenes/Player/Civilian/low/jeers01.vcd" 
	scene "scenes/Player/Civilian/low/jeers02.vcd" 
	scene "scenes/Player/Civilian/low/jeers03.vcd" 
	scene "scenes/Player/Civilian/low/jeers04.vcd" 
	scene "scenes/Player/Civilian/low/jeers05.vcd" 
	scene "scenes/Player/Civilian/low/jeers06.vcd" 
	scene "scenes/Player/Civilian/low/jeers07.vcd" 
	scene "scenes/Player/Civilian/low/jeers08.vcd" 
	scene "scenes/Player/Civilian/low/jeers09.vcd" 
	scene "scenes/Player/Civilian/low/jeers10.vcd" 
	scene "scenes/Player/Civilian/low/jeers11.vcd" 
	scene "scenes/Player/Civilian/low/jeers12.vcd" 
}
Rule PlayerSuddenDeathCivilian
{
	criteria ConceptPlayerSuddenDeathStart IsCivilian
	Response PlayerSuddenDeathCivilian
}

Response PlayerStalemateCivilian
{
	scene "scenes/Player/Civilian/low/autodejectedtie01.vcd" 
	scene "scenes/Player/Civilian/low/autodejectedtie02.vcd" 
	scene "scenes/Player/Civilian/low/autodejectedtie03.vcd" 
}
Rule PlayerStalemateCivilian
{
	criteria ConceptPlayerStalemate IsCivilian
	Response PlayerStalemateCivilian
}

Response PlayerTeleporterThanksCivilian
{
	scene "scenes/Player/Civilian/low/ThanksForTheTeleporter01.vcd" 
	scene "scenes/Player/Civilian/low/ThanksForTheTeleporter02.vcd" 
	scene "scenes/Player/Civilian/low/ThanksForTheTeleporter03.vcd" 
}
Rule PlayerTeleporterThanksCivilian
{
	criteria ConceptTeleported IsNotEngineer IsCivilian 30PercentChance
	Response PlayerTeleporterThanksCivilian
}


//--------------------------------------------------------------------------------------------------------------
// Auto Speech Combat
//--------------------------------------------------------------------------------------------------------------
Response KilledPlayerManyCivilian
{
	scene "scenes/Player/Civilian/low/specialcompleted03.vcd" 
	scene "scenes/Player/Civilian/low/taunts01.vcd" 
	scene "scenes/Player/Civilian/low/taunts02.vcd" 
	scene "scenes/Player/Civilian/low/taunts09.vcd" 
	scene "scenes/Player/Civilian/low/taunts10.vcd" 
}
Rule KilledPlayerManyCivilian
{
	criteria ConceptKilledPlayer IsManyRecentKills 30PercentChance IsWeaponPrimary KilledPlayerDelay CivilianNotKillSpeech IsCivilian
	ApplyContext "CivilianKillSpeech:1:10"
	applycontexttoworld
	Response KilledPlayerManyCivilian
}

Response KilledPlayerMeleeCivilian
{
	scene "scenes/Player/Civilian/low/specialcompleted05.vcd" 
}
Rule KilledPlayerMeleeCivilian
{
	criteria ConceptKilledPlayer KilledPlayerDelay 30PercentChance  IsWeaponMelee CivilianNotKillSpeechMelee IsCivilian
	ApplyContext "CivilianKillSpeechMelee:1:10"
	applycontexttoworld
	Response KilledPlayerMeleeCivilian
}

Response KilledPlayerVeryManyCivilian
{
	scene "scenes/Player/Civilian/low/taunts17.vcd" 
}
Rule KilledPlayerVeryManyCivilian
{
	criteria ConceptKilledPlayer IsVeryManyRecentKills 50PercentChance IsWeaponPrimary KilledPlayerDelay CivilianNotKillSpeech IsCivilian
	ApplyContext "CivilianKillSpeech:1:10"
	applycontexttoworld
	Response KilledPlayerVeryManyCivilian
}

Response PlayerKilledCapperCivilian
{
	scene "scenes/Player/Civilian/low/cheers04.vcd" 
	scene "scenes/Player/Civilian/low/cheers05.vcd" 
	scene "scenes/Player/Civilian/low/cheers06.vcd" 
}
Rule PlayerKilledCapperCivilian
{
	criteria ConceptCapBlocked IsCivilian
	ApplyContext "CivilianKillSpeech:1:10"
	Response PlayerKilledCapperCivilian
}

Response PlayerKilledDominatingCivilian
{
	scene "scenes/Player/Civilian/low/laughevil01.vcd" predelay "2.5"
	scene "scenes/Player/Civilian/low/laughevil02.vcd" predelay "2.5"
	scene "scenes/Player/Civilian/low/laughevil03.vcd" predelay "2.5"
	scene "scenes/Player/Civilian/low/laughhappy01.vcd" predelay "2.5"
	scene "scenes/Player/Civilian/low/laughhappy02.vcd" predelay "2.5"
	scene "scenes/Player/Civilian/low/laughhappy03.vcd" predelay "2.5"
	scene "scenes/Player/Civilian/low/laughlong01.vcd" predelay "2.5"
	scene "scenes/Player/Civilian/low/laughlong03.vcd" predelay "2.5"
	scene "scenes/Player/Civilian/low/taunts07.vcd" predelay "2.5"
	scene "scenes/Player/Civilian/low/taunts13.vcd" predelay "2.5"
}
Rule PlayerKilledDominatingCivilian
{
	criteria ConceptKilledPlayer IsCivilian IsDominated
	ApplyContext "CivilianKillSpeech:1:10"
	Response PlayerKilledDominatingCivilian
}

Response PlayerKilledForRevengeCivilian
{
	scene "scenes/Player/Civilian/low/battlecry06.vcd" predelay "2.5"
	scene "scenes/Player/Civilian/low/cheers01.vcd" predelay "2.5"
	scene "scenes/Player/Civilian/low/goodjob02.vcd" predelay "2.5"
}
Rule PlayerKilledForRevengeCivilian
{
	criteria ConceptKilledPlayer IsCivilian IsRevenge
	ApplyContext "CivilianKillSpeech:1:10"
	Response PlayerKilledForRevengeCivilian
}

Response PlayerKilledObjectCivilian
{
	scene "scenes/Player/Civilian/low/battlecry01.vcd" 
	scene "scenes/Player/Civilian/low/positivevocalization01.vcd" 
	scene "scenes/Player/Civilian/low/positivevocalization02.vcd" 
	scene "scenes/Player/Civilian/low/positivevocalization03.vcd" 
	scene "scenes/Player/Civilian/low/specialcompleted04.vcd" 
}
Rule PlayerKilledObjectCivilian
{
	criteria ConceptKilledObject IsCivilian 30PercentChance IsARecentKill
	ApplyContext "CivilianKillSpeechObject:1:30"
	applycontexttoworld
	Response PlayerKilledObjectCivilian
}


//--------------------------------------------------------------------------------------------------------------
// Auto Speech Pain
//--------------------------------------------------------------------------------------------------------------
Response PlayerAttackerPainCivilian
{
	scene "scenes/Player/Civilian/low/painsevere01.vcd" 
	scene "scenes/Player/Civilian/low/painsevere02.vcd" 
	scene "scenes/Player/Civilian/low/painsevere03.vcd" 
	scene "scenes/Player/Civilian/low/painsevere04.vcd" 
	scene "scenes/Player/Civilian/low/painsevere05.vcd" 
	scene "scenes/Player/Civilian/low/painsevere06.vcd" 
}
Rule PlayerAttackerPainCivilian
{
	criteria ConceptAttackerPain IsCivilian
	Response PlayerAttackerPainCivilian
}

Response PlayerOnFireCivilian
{
	scene "scenes/Player/Civilian/low/autoonfire01.vcd" 
}
Rule PlayerOnFireCivilian
{
	criteria ConceptFire IsCivilian CivilianIsNotStillonFire
	ApplyContext "CivilianOnFire:1:7"
	Response PlayerOnFireCivilian
}

Response PlayerOnFireRareCivilian
{
	scene "scenes/Player/Civilian/low/autoonfire02.vcd" 
	scene "scenes/Player/Civilian/low/autoonfire03.vcd" 
}
Rule PlayerOnFireRareCivilian
{
	criteria ConceptFire IsCivilian 10PercentChance CivilianIsNotStillonFire
	ApplyContext "CivilianOnFire:1:7"
	Response PlayerOnFireRareCivilian
}

Response PlayerPainCivilian
{
	scene "scenes/Player/Civilian/low/painsharp01.vcd" 
	scene "scenes/Player/Civilian/low/painsharp02.vcd" 
	scene "scenes/Player/Civilian/low/painsharp03.vcd" 
	scene "scenes/Player/Civilian/low/painsharp04.vcd" 
	scene "scenes/Player/Civilian/low/painsharp05.vcd" 
	scene "scenes/Player/Civilian/low/painsharp06.vcd" 
	scene "scenes/Player/Civilian/low/painsharp07.vcd" 
	scene "scenes/Player/Civilian/low/painsharp08.vcd" 
}
Rule PlayerPainCivilian
{
	criteria ConceptPain IsCivilian
	Response PlayerPainCivilian
}

Response PlayerStillOnFireCivilian
{
	scene "scenes/Player/Civilian/low/autoonfiresecondary01.vcd" 
}
Rule PlayerStillOnFireCivilian
{
	criteria ConceptFire IsCivilian  CivilianIsStillonFire
	ApplyContext "CivilianOnFire:1:7"
	Response PlayerStillOnFireCivilian
}


//--------------------------------------------------------------------------------------------------------------
// Speech Menu 1
//--------------------------------------------------------------------------------------------------------------
Response PlayerGoCivilian
{
	scene "scenes/Player/Civilian/low/go01.vcd" 
	scene "scenes/Player/Civilian/low/go02.vcd" 
	scene "scenes/Player/Civilian/low/go03.vcd" 
}
Rule PlayerGoCivilian
{
	criteria ConceptPlayerGo IsCivilian
	Response PlayerGoCivilian
}

Response PlayerHeadLeftCivilian
{
	scene "scenes/Player/Civilian/low/headleft01.vcd" 
	scene "scenes/Player/Civilian/low/headleft02.vcd" 
	scene "scenes/Player/Civilian/low/headleft03.vcd" 
}
Rule PlayerHeadLeftCivilian
{
	criteria ConceptPlayerLeft  IsCivilian
	Response PlayerHeadLeftCivilian
}

Response PlayerHeadRightCivilian
{
	scene "scenes/Player/Civilian/low/headright01.vcd" 
	scene "scenes/Player/Civilian/low/headright02.vcd" 
	scene "scenes/Player/Civilian/low/headright03.vcd" 
}
Rule PlayerHeadRightCivilian
{
	criteria ConceptPlayerRight  IsCivilian
	Response PlayerHeadRightCivilian
}

Response PlayerHelpCivilian
{
	scene "scenes/Player/Civilian/low/helpme01.vcd" 
	scene "scenes/Player/Civilian/low/helpme02.vcd" 
	scene "scenes/Player/Civilian/low/helpme03.vcd" 
}
Rule PlayerHelpCivilian
{
	criteria ConceptPlayerHelp IsCivilian
	Response PlayerHelpCivilian
}

Response PlayerHelpCaptureCivilian
{
	scene "scenes/Player/Civilian/low/helpmecapture01.vcd" 
	scene "scenes/Player/Civilian/low/helpmecapture02.vcd" 
	scene "scenes/Player/Civilian/low/helpmecapture03.vcd" 
}
Rule PlayerHelpCaptureCivilian
{
	criteria ConceptPlayerHelp IsCivilian IsOnCappableControlPoint
	ApplyContext "CivilianHelpCap:1:10"
	Response PlayerHelpCaptureCivilian
}

Response PlayerHelpCapture2Civilian
{
	scene "scenes/Player/Civilian/low/standonthepoint01.vcd" 
	scene "scenes/Player/Civilian/low/standonthepoint02.vcd" 
	scene "scenes/Player/Civilian/low/standonthepoint03.vcd" 
}
Rule PlayerHelpCapture2Civilian
{
	criteria ConceptPlayerHelp IsCivilian IsOnCappableControlPoint IsHelpCapCivilian
	Response PlayerHelpCapture2Civilian
}

Response PlayerHelpDefendCivilian
{
	scene "scenes/Player/Civilian/low/helpmedefend01.vcd" 
	scene "scenes/Player/Civilian/low/helpmedefend02.vcd" 
	scene "scenes/Player/Civilian/low/helpmedefend03.vcd" 
	scene "scenes/Player/Civilian/low/helpmedefend04.vcd" 
}
Rule PlayerHelpDefendCivilian
{
	criteria ConceptPlayerHelp IsCivilian IsOnFriendlyControlPoint
	Response PlayerHelpDefendCivilian
}

Response PlayerMedicCivilian
{
	scene "scenes/Player/Civilian/low/medic01.vcd" 
	scene "scenes/Player/Civilian/low/medic02.vcd" 
	scene "scenes/Player/Civilian/low/medic03.vcd" 
}
Rule PlayerMedicCivilian
{
	criteria ConceptPlayerMedic IsCivilian
	Response PlayerMedicCivilian
}

Response PlayerMoveUpCivilian
{
	scene "scenes/Player/Civilian/low/moveup01.vcd" 
	scene "scenes/Player/Civilian/low/moveup02.vcd" 
	scene "scenes/Player/Civilian/low/moveup03.vcd" 
}
Rule PlayerMoveUpCivilian
{
	criteria ConceptPlayerMoveUp  IsCivilian
	Response PlayerMoveUpCivilian
}

Response PlayerNoCivilian
{
	scene "scenes/Player/Civilian/low/no01.vcd" 
	scene "scenes/Player/Civilian/low/no02.vcd" 
	scene "scenes/Player/Civilian/low/no03.vcd" 
}
Rule PlayerNoCivilian
{
	criteria ConceptPlayerNo  IsCivilian
	Response PlayerNoCivilian
}

Response PlayerThanksCivilian
{
	scene "scenes/Player/Civilian/low/Thanks01.vcd" 
	scene "scenes/Player/Civilian/low/Thanks02.vcd" 
}
Rule PlayerThanksCivilian
{
	criteria ConceptPlayerThanks IsCivilian
	Response PlayerThanksCivilian
}

Response PlayerYesCivilian
{
	scene "scenes/Player/Civilian/low/Yes01.vcd" 
	scene "scenes/Player/Civilian/low/Yes02.vcd" 
	scene "scenes/Player/Civilian/low/Yes03.vcd" 
	scene "scenes/Player/Civilian/low/Yes04.vcd" 
}
Rule PlayerYesCivilian
{
	criteria ConceptPlayerYes  IsCivilian
	Response PlayerYesCivilian
}


//--------------------------------------------------------------------------------------------------------------
// Speech Menu 2
//--------------------------------------------------------------------------------------------------------------
Response PlayerActivateChargeCivilian
{
	scene "scenes/Player/Civilian/low/activatecharge01.vcd" 
	scene "scenes/Player/Civilian/low/activatecharge02.vcd" 
	scene "scenes/Player/Civilian/low/activatecharge03.vcd" 
}
Rule PlayerActivateChargeCivilian
{
	criteria ConceptPlayerActivateCharge IsCivilian
	Response PlayerActivateChargeCivilian
}

Response PlayerCloakedSpyCivilian
{
	scene "scenes/Player/Civilian/low/cloakedspy01.vcd" 
	scene "scenes/Player/Civilian/low/cloakedspy02.vcd" 
	scene "scenes/Player/Civilian/low/cloakedspy03.vcd" 
}
Rule PlayerCloakedSpyCivilian
{
	criteria ConceptPlayerCloakedSpy IsCivilian
	Response PlayerCloakedSpyCivilian
}

Response PlayerDispenserHereCivilian
{
	scene "scenes/Player/Civilian/low/needdispenser01.vcd" 
}
Rule PlayerDispenserHereCivilian
{
	criteria ConceptPlayerDispenserHere IsCivilian
	Response PlayerDispenserHereCivilian
}

Response PlayerIncomingCivilian
{
	scene "scenes/Player/Civilian/low/incoming01.vcd" 
}
Rule PlayerIncomingCivilian
{
	criteria ConceptPlayerIncoming IsCivilian
	Response PlayerIncomingCivilian
}

Response PlayerSentryAheadCivilian
{
	scene "scenes/Player/Civilian/low/sentryahead01.vcd" 
	scene "scenes/Player/Civilian/low/sentryahead02.vcd" 
	scene "scenes/Player/Civilian/low/sentryahead03.vcd" 
}
Rule PlayerSentryAheadCivilian
{
	criteria ConceptPlayerSentryAhead IsCivilian
	Response PlayerSentryAheadCivilian
}

Response PlayerSentryHereCivilian
{
	scene "scenes/Player/Civilian/low/needsentry01.vcd" 
}
Rule PlayerSentryHereCivilian
{
	criteria ConceptPlayerSentryHere IsCivilian
	Response PlayerSentryHereCivilian
}

Response PlayerTeleporterHereCivilian
{
	scene "scenes/Player/Civilian/low/needteleporter01.vcd" 
}
Rule PlayerTeleporterHereCivilian
{
	criteria ConceptPlayerTeleporterHere IsCivilian
	Response PlayerTeleporterHereCivilian
}


//--------------------------------------------------------------------------------------------------------------
// Speech Menu 3
//--------------------------------------------------------------------------------------------------------------
Response PlayerBattleCryCivilian
{
	scene "scenes/Player/Civilian/low/battlecry01.vcd" 
	scene "scenes/Player/Civilian/low/battlecry02.vcd" 
	scene "scenes/Player/Civilian/low/battlecry03.vcd" 
	scene "scenes/Player/Civilian/low/battlecry04.vcd" 
	scene "scenes/Player/Civilian/low/battlecry05.vcd" 
	scene "scenes/Player/Civilian/low/battlecry06.vcd" 
}
Rule PlayerBattleCryCivilian
{
	criteria ConceptPlayerBattleCry IsCivilian
	Response PlayerBattleCryCivilian
}

Response PlayerCheersCivilian
{
	scene "scenes/Player/Civilian/low/cheers01.vcd" 
	scene "scenes/Player/Civilian/low/cheers02.vcd" 
	scene "scenes/Player/Civilian/low/cheers03.vcd" 
	scene "scenes/Player/Civilian/low/cheers04.vcd" 
	scene "scenes/Player/Civilian/low/cheers05.vcd" 
	scene "scenes/Player/Civilian/low/cheers06.vcd" 
}
Rule PlayerCheersCivilian
{
	criteria ConceptPlayerCheers IsCivilian
	Response PlayerCheersCivilian
}

Response PlayerGoodJobCivilian
{
	scene "scenes/Player/Civilian/low/goodjob01.vcd" 
	scene "scenes/Player/Civilian/low/goodjob02.vcd" 
	scene "scenes/Player/Civilian/low/goodjob03.vcd" 
}
Rule PlayerGoodJobCivilian
{
	criteria ConceptPlayerGoodJob IsCivilian
	Response PlayerGoodJobCivilian
}

Response PlayerJeersCivilian
{
	scene "scenes/Player/Civilian/low/jeers01.vcd" 
	scene "scenes/Player/Civilian/low/jeers02.vcd" 
	scene "scenes/Player/Civilian/low/jeers03.vcd" 
	scene "scenes/Player/Civilian/low/jeers04.vcd" 
	scene "scenes/Player/Civilian/low/jeers05.vcd" 
	scene "scenes/Player/Civilian/low/jeers06.vcd" 
	scene "scenes/Player/Civilian/low/jeers07.vcd" 
	scene "scenes/Player/Civilian/low/jeers08.vcd" 
	scene "scenes/Player/Civilian/low/jeers09.vcd" 
	scene "scenes/Player/Civilian/low/jeers10.vcd" 
	scene "scenes/Player/Civilian/low/jeers11.vcd" 
	scene "scenes/Player/Civilian/low/jeers12.vcd" 
}
Rule PlayerJeersCivilian
{
	criteria ConceptPlayerJeers IsCivilian
	Response PlayerJeersCivilian
}

Response PlayerLostPointCivilian
{
	scene "scenes/Player/Civilian/low/negativevocalization01.vcd" 
	scene "scenes/Player/Civilian/low/negativevocalization02.vcd" 
	scene "scenes/Player/Civilian/low/negativevocalization03.vcd" 
	scene "scenes/Player/Civilian/low/negativevocalization04.vcd" 
	scene "scenes/Player/Civilian/low/negativevocalization05.vcd" 
	scene "scenes/Player/Civilian/low/negativevocalization06.vcd" 
}
Rule PlayerLostPointCivilian
{
	criteria ConceptPlayerLostPoint IsCivilian
	Response PlayerLostPointCivilian
}

Response PlayerNegativeCivilian
{
	scene "scenes/Player/Civilian/low/negativevocalization01.vcd" 
	scene "scenes/Player/Civilian/low/negativevocalization02.vcd" 
	scene "scenes/Player/Civilian/low/negativevocalization03.vcd" 
	scene "scenes/Player/Civilian/low/negativevocalization04.vcd" 
	scene "scenes/Player/Civilian/low/negativevocalization05.vcd" 
	scene "scenes/Player/Civilian/low/negativevocalization06.vcd" 
}
Rule PlayerNegativeCivilian
{
	criteria ConceptPlayerNegative IsCivilian
	Response PlayerNegativeCivilian
}

Response PlayerNiceShotCivilian
{
	scene "scenes/Player/Civilian/low/niceshot01.vcd" 
	scene "scenes/Player/Civilian/low/niceshot02.vcd" 
	scene "scenes/Player/Civilian/low/niceshot03.vcd" 
}
Rule PlayerNiceShotCivilian
{
	criteria ConceptPlayerNiceShot IsCivilian
	Response PlayerNiceShotCivilian
}

Response PlayerPositiveCivilian
{
	scene "scenes/Player/Civilian/low/positivevocalization01.vcd" 
	scene "scenes/Player/Civilian/low/positivevocalization02.vcd" 
	scene "scenes/Player/Civilian/low/positivevocalization03.vcd" 
	scene "scenes/Player/Civilian/low/positivevocalization04.vcd" 
	scene "scenes/Player/Civilian/low/positivevocalization05.vcd" 
}
Rule PlayerPositiveCivilian
{
	criteria ConceptPlayerPositive IsCivilian
	Response PlayerPositiveCivilian
}

Response PlayerTauntsCivilian
{
	scene "scenes/Player/Civilian/low/laughlong02.vcd" 
	scene "scenes/Player/Civilian/low/laughshort01.vcd" 
	scene "scenes/Player/Civilian/low/laughshort02.vcd" 
	scene "scenes/Player/Civilian/low/laughshort03.vcd" 
	scene "scenes/Player/Civilian/low/laughshort04.vcd" 
}
Rule PlayerTauntsCivilian
{
	criteria ConceptPlayerTaunts IsCivilian
	Response PlayerTauntsCivilian
}

